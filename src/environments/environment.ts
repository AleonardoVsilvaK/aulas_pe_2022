// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDH0K-uFDpfuNIGCf1E6cL0bbhETDdWp0A',
    authDomain: 'controle-if-leo.firebaseapp.com',
    projectId: 'controle-if-leo',
    storageBucket: 'controle-if-leo.appspot.com',
    messagingSenderId: '435061134481',
    appId: '1:435061134481:web:a996da071b0057c09a1b30',
    measurementId: 'G-6XD2XXMQEX'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
