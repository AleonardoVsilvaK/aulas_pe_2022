import { Injectable } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {} User } from "firebase/compat/app";


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  [x: string]: any;


  isLoggednIn: Observable<User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController
  ) { 
    this.isLoggednIn = auth.authState;
  }


  login(user){

    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')).
    catch(() => this.showError());
  }

  private async showError(){
    const ctrl = await this.toast.create({
      message: 'Dados Incorretos',
      duration: 3000
    });

    ctrl.present();
  }

  recoverPass(data){
    this.auth.sendPasswordResetEmail(data.email).
    then(() => this.nav.navigateBack('auth')).
    catch(err => {

    });
  }

  createUser(user){
    this.auth.createUserWithEmailAndPassword(user.email, user.password);
  }

}

  logout()
    this.auth.signOut().
    then(() => this.nav.navigateBack('auth'));
  }

